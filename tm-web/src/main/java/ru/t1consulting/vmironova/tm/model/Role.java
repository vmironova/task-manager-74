package ru.t1consulting.vmironova.tm.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.t1consulting.vmironova.tm.enumerated.RoleType;

import javax.persistence.*;
import java.util.UUID;

@Getter
@Setter
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "tm_role")
public final class Role {

    @Id
    @Column
    @NotNull
    private String id = UUID.randomUUID().toString();

    @NotNull
    @ManyToOne
    private User user;

    @NotNull
    @Column(name = "roletype")
    @Enumerated(EnumType.STRING)
    private RoleType roleType = RoleType.USER;

    @NotNull
    @Override
    public String toString() {
        return roleType.name();
    }

}
