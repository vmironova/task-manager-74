package ru.t1consulting.vmironova.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.t1consulting.vmironova.tm.api.endpoint.TaskEndpoint;
import ru.t1consulting.vmironova.tm.api.service.dto.ITaskDTOService;
import ru.t1consulting.vmironova.tm.dto.model.TaskDTO;
import ru.t1consulting.vmironova.tm.util.UserUtil;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@RestController
@RequestMapping("/api/tasks")
@WebService(endpointInterface = "ru.t1consulting.vmironova.tm.api.endpoint.TaskEndpoint")
public class TaskRestEndpointImpl implements TaskEndpoint {

    @Autowired
    private ITaskDTOService taskService;

    @Override
    @WebMethod
    @GetMapping("/findAll")
    public List<TaskDTO> findAll() throws Exception {
        return taskService.findAllByUserId(UserUtil.getUserId());
    }

    @Nullable
    @Override
    @WebMethod
    @GetMapping("/findAllByProjectId/{projectId}")
    public List<TaskDTO> findAllByProjectId(
            @WebParam(name = "projectId", partName = "projectId")
            @PathVariable("projectId") final @NotNull String projectId
    ) throws Exception {
        return taskService.findAllByUserIdAndProjectId(UserUtil.getUserId(), projectId);
    }

    @NotNull
    @Override
    @WebMethod
    @PostMapping("/add")
    public TaskDTO add(
            @WebParam(name = "task", partName = "task")
            @RequestBody final @NotNull TaskDTO task
    ) throws Exception {
        return taskService.addByUserId(UserUtil.getUserId(), task);
    }

    @NotNull
    @Override
    @WebMethod
    @PostMapping("/save")
    public TaskDTO save(
            @WebParam(name = "task", partName = "task")
            @RequestBody final @NotNull TaskDTO task
    ) throws Exception {
        return taskService.updateByUserId(UserUtil.getUserId(), task);
    }

    @Nullable
    @Override
    @WebMethod
    @GetMapping("/findById/{id}")
    public TaskDTO findById(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") final @NotNull String id
    ) throws Exception {
        return taskService.findOneByUserIdAndId(UserUtil.getUserId(), id);
    }

    @Override
    @WebMethod
    @GetMapping("/existsById/{id}")
    public boolean existsById(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") final @NotNull String id
    ) throws Exception {
        return taskService.existsByUserIdAndId(UserUtil.getUserId(), id);
    }

    @Override
    @WebMethod
    @GetMapping("/count")
    public long count() throws Exception {
        return taskService.countByUserId(UserUtil.getUserId());
    }

    @Override
    @WebMethod
    @PostMapping("/deleteById/{id}")
    public void deleteById(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") final @NotNull String id
    ) throws Exception {
        taskService.removeByUserIdAndId(UserUtil.getUserId(), id);
    }

    @Override
    @WebMethod
    @PostMapping("/delete")
    public void delete(
            @WebParam(name = "task", partName = "task")
            @RequestBody final @NotNull TaskDTO task
    ) throws Exception {
        taskService.removeByUserId(UserUtil.getUserId(), task);
    }

    @Override
    @WebMethod
    @PostMapping("/deleteAll")
    public void clear() throws Exception {
        taskService.clearByUserId(UserUtil.getUserId());
    }

}
