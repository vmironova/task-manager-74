package ru.t1consulting.vmironova.tm.component;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;
import ru.t1consulting.vmironova.tm.api.endpoint.*;
import ru.t1consulting.vmironova.tm.api.service.ILoggerService;
import ru.t1consulting.vmironova.tm.api.service.IPropertyService;
import ru.t1consulting.vmironova.tm.api.service.IServiceLocator;
import ru.t1consulting.vmironova.tm.api.service.ITokenService;
import ru.t1consulting.vmironova.tm.event.ConsoleEvent;
import ru.t1consulting.vmironova.tm.exception.system.ArgumentNotSupportedException;
import ru.t1consulting.vmironova.tm.listener.AbstractListener;
import ru.t1consulting.vmironova.tm.util.SystemUtil;
import ru.t1consulting.vmironova.tm.util.TerminalUtil;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

@Component
public final class Bootstrap implements IServiceLocator {

    @Nullable
    @Autowired
    private AbstractListener[] listeners;

    @NotNull
    @Autowired
    private ApplicationEventPublisher publisher;

    @Getter
    @NotNull
    @Autowired
    private ILoggerService loggerService;

    @Getter
    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @Getter
    @NotNull
    @Autowired
    private ITokenService tokenService;

    @Getter
    @NotNull
    @Autowired
    private IAuthEndpoint authEndpointClient;

    @Getter
    @NotNull
    @Autowired
    private ISystemEndpoint systemEndpointClient;

    @Getter
    @NotNull
    @Autowired
    private IProjectEndpoint projectEndpointClient;

    @Getter
    @NotNull
    @Autowired
    private ITaskEndpoint taskEndpointClient;

    @Getter
    @NotNull
    @Autowired
    private IUserEndpoint userEndpointClient;

    @SneakyThrows
    private void initPID() {
        @NotNull final String fileName = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(fileName), pid.getBytes());
        @NotNull final File file = new File(fileName);
        file.deleteOnExit();
    }

    private void prepareStartup() {
        initPID();
        loggerService.info("** WELCOME TO TASK-MANAGER **");
        Runtime.getRuntime().addShutdownHook(new Thread(this::prepareShutdown));
    }

    private void prepareShutdown() {
        loggerService.info("** TASK-MANAGER IS SHUTTING DOWN **");
    }

    public boolean processArguments(@Nullable final String[] args) {
        if (args == null || args.length == 0) return false;
        @Nullable final String arg = args[0];
        processArgument(arg);
        return true;
    }

    private void processArgument(@Nullable final String argument) {
        @Nullable final AbstractListener listener = getListenerByArgument(argument);
        if (listener == null) throw new ArgumentNotSupportedException(argument);
        publisher.publishEvent(new ConsoleEvent(listener.getName()));
    }

    @Nullable
    private AbstractListener getListenerByArgument(@Nullable final String argument) {
        for (@Nullable final AbstractListener listener : listeners) {
            if (argument.equals(listener.getArgument())) return listener;
        }
        return null;
    }

    public void run(@Nullable final String[] args) {
        if (processArguments(args)) System.exit(0);
        prepareStartup();
        while (!Thread.currentThread().isInterrupted()) {
            try {
                System.out.println("ENTER COMMAND:");
                @NotNull final String command = TerminalUtil.nextLine();
                loggerService.command(command);
                publisher.publishEvent(new ConsoleEvent(command));
                System.out.println("[OK]");
            } catch (@NotNull final Exception e) {
                loggerService.error(e);
                System.out.println("[FAIL]");
            }
        }
    }

}
