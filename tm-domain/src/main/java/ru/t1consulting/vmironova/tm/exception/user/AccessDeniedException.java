package ru.t1consulting.vmironova.tm.exception.user;

public final class AccessDeniedException extends AbstractUserException {

    public AccessDeniedException() {
        super("Error! Access denied.");
    }

}
